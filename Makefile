MAKEFILE_PATH := $(abspath $(lastword $(MAKEFILE_LIST)))
WORK_DIR := $(CURDIR)

default: all

# Change the path to Z3 4.1.1 accordingly
# The directory indicated by this path should contain "lib" and "bin"
# e.g. "/home/z3_src_4.1.1"
Z3_path = ./z3_src_4.1.1

#-----------------------------------------

JUNK = str str_debug str_dynamc
SOURCE = strAstReduce.cpp strArgmt.cpp strRegex.cpp strTheory.cpp strMain.cpp
INCLUDE = $(Z3_path)/lib
LIB = $(Z3_path)/bin/external

dynamic: $(SOURCE)
	/usr/local/bin/g++-4.9 -O3 -std=c++11  -I$(INCLUDE) -L$(LIB) $(SOURCE) -lz3  -o str_dynamc 

all: $(SOURCE)
	@echo ">> Z3 Source Dir: "$(Z3_path)
	@echo ""
	/usr/local/bin/g++-4.9 -ggdb -g -std=c++11 -D DEBUGLOG   -I$(INCLUDE) -L$(LIB) $(SOURCE) -lz3 -o str_debug	
	@echo ""
	/usr/local/bin/g++-4.9 -O3 -std=c++11  -I$(INCLUDE) -L$(LIB) $(SOURCE) -lz3  -o str -Wall
	@echo ""

	
clean:
	rm -f $(JUNK)
